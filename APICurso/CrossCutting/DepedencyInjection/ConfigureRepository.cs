﻿using Data.Context;
using Data.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using Repository.Interface;

namespace CrossCutting.DepedencyInjection
{
    public class ConfigureRepository
    {
        public static void ConfigureDepedenciesRepository(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
            serviceCollection.AddScoped<IUserRepository, UserRepository>();

            //    serviceCollection.AddDbContext<MyContext>(
            //    options => options.UseMySql("Server=localhost;Port=5101;Database=dbApi;Uid=root;Pwd=mysql"));
            //}

            serviceCollection.AddDbContext<MyContext>(
            options => options.UseSqlServer("Data Source=DESKTOP-V244N3R\\MSSQLSERVER01;Database=dbAPI;User ID=sa;Password=desenv;"));
        }
}
}