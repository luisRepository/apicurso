﻿using AutoMapper;
using Domain.Dto;
using Domain.Models;

namespace CrossCutting.Mappings
{
    public class DtoToModelProfile: Profile
    {
        public DtoToModelProfile()
        {
            CreateMap<UserModel, UserDto>()
                .ReverseMap();

            CreateMap<UserModel, UserDtoCreate>()
               .ReverseMap();

            CreateMap<UserModel, UserDtoUpdate>()
               .ReverseMap();
        }
    }
}