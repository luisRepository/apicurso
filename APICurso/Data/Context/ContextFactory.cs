﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Data.Context
{
    public class ContextFactory : IDesignTimeDbContextFactory<MyContext>
    {
        public MyContext CreateDbContext(string[] args)
        {
            //var connectionString = "Server=localhost;Port=5101;Database=dbApi;Uid=root;Pwd=mysql";
            var connectionString = "Data Source=DESKTOP-V244N3R\\MSSQLSERVER01;Database=dbAPI;User ID=sa;Password=desenv;";
            var optionsBuider = new DbContextOptionsBuilder<MyContext>();
            //optionsBuider.UseMySql(connectionString);
            optionsBuider.UseSqlServer(connectionString);
            return new MyContext(optionsBuider.Options);

        }
    }
}