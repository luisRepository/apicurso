﻿using Data.Context;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Repository.Interface;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class UserRepository : BaseRepository<UserEntity>, IUserRepository
    {
        private DbSet<UserEntity> dataset;

        public UserRepository(MyContext context) : base(context)
        {
            this.dataset = context.Set<UserEntity>();
        }

        public async Task<UserEntity> FindByLogin(string email)
        {
            return await dataset.FirstOrDefaultAsync(u => u.Email.Equals(email));
        }
    }
}