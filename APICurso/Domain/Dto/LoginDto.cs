﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Dto
{
    public class LoginDto
    {
        [Required(ErrorMessage = "Email é campo obrigatório")]
        [EmailAddress(ErrorMessage ="E-mail em formato inválido")]
        [MaxLength(100, ErrorMessage ="Email deve ter no máximo {1} caracteres")]
        public string Email { get; set; }
    }
}