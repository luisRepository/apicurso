﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Dto
{
    public class UserDtoCreate
    {

        [Required(ErrorMessage = "Nome é campo obrigatório")]
        [MaxLength(60, ErrorMessage = "Nome deve ter no máximo {1} caracteres")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email é campo obrigatório")]
        [EmailAddress(ErrorMessage = "E-mail em formato inválido")]
        [MaxLength(100, ErrorMessage = "Email deve ter no máximo {1} caracteres")]
        public string Email { get; set; }
    }
}