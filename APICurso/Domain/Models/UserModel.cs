﻿using System;

namespace Domain.Models
{
    public class UserModel
    {
        private Guid id;

        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private DateTime createAt;

        public DateTime CreateAt
        {
            get { return createAt; }
            set {
                createAt = value == null  ? DateTime.UtcNow : value ;
            }
        }

        private DateTime updateAt;

        public DateTime UpdateAt
        {
            get { return updateAt; }
            set { updateAt = value; }
        }

    }
}