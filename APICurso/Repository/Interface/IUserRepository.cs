﻿using Domain.Entities;
using System.Threading.Tasks;

namespace Repository.Interface
{
    public interface IUserRepository: IRepository<UserEntity>
    {
        Task<UserEntity> FindByLogin(string email);
    }
}