﻿using Domain.Dto;
using System.Threading.Tasks;

namespace Service.Interfaces
{
    public interface ILoginService
    {
        Task<object> FindByLogin(LoginDto user);
    }
}