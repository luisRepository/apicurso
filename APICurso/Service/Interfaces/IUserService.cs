﻿using Domain.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> GetUser(Guid id);

        Task<IEnumerable<UserDto>> GetAllUser();

        Task<UserDtoCreateResult> PostUser(UserDtoCreate user);

        Task<UserDtoUpdateResult> PutUser(UserDtoUpdate user);

        Task<bool> DeleteUser(Guid id);
    }
}