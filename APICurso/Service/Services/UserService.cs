﻿using AutoMapper;
using Domain.Dto;
using Domain.Entities;
using Domain.Models;
using Repository;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<UserEntity> repository;
        private readonly IMapper mapper;

        public UserService(IRepository<UserEntity> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<UserDto> GetUser(Guid id)
        {
            var entity = await repository.SelectAsync(id);
            return mapper.Map<UserDto>(entity) ?? new UserDto();
        }

        public async Task<IEnumerable<UserDto>> GetAllUser()
        {
            var listEntity = await repository.SelectAsync();
            return mapper.Map<IEnumerable<UserDto>>(listEntity);
        }

        public async Task<UserDtoCreateResult> PostUser(UserDtoCreate user)
        {
            var model = mapper.Map<UserModel>(user);
            var entity = mapper.Map<UserEntity>(model);
            var result = await repository.InsertAsync(entity);
            return mapper.Map<UserDtoCreateResult>(result);
        }

        public async Task<UserDtoUpdateResult> PutUser(UserDtoUpdate user)
        {
            var model = mapper.Map<UserModel>(user);
            var entity = mapper.Map<UserEntity>(model);
            var result = await repository.UpdateAsync(entity);
            return mapper.Map<UserDtoUpdateResult>(result);
        }

        public async Task<bool> DeleteUser(Guid id)
        {
            return await repository.DeleteAsync(id);
        }
    }
}